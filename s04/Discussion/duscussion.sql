-- Insertion Multiple row
INSERT INTO artist (name) VALUES ("Taylor Swift"), ("Lady Gaga"), ("Justine Bieber"), ("Ariana Grande"), ("Bruno Mars");


/* 
Taylor Swift - 3
Lady Gaga - 4
Justine Bieber - 5
Ariana Grande - 6
Bruno Mars - 7

 */

-- Taylor Swift
INSERT INTO albums (albums_title, date_released, artist_id) VALUES ("Fearless", "2008-01-01", 3); -- artist id 3;
-- get the fearless id from the albumn
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Fearless", 246, "Pop Rock", 3),
("Love story", 213, "Country pop", 3); --album id 3


INSERT INTO albums (albums_title, date_released, artist_id) VALUES ("Red", "2012-01-01", 3);
-- get the Red id from album
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("State of Grace", 250, "Rock, alternative rock, arena rock", 4),
("Red", 204, "Country", 4); -- album id 4


-- lady gaga
INSERT INTO albums (albums_title, date_released, artist_id) VALUES ("A star a born", "2018-01-01", 4); -- artist id 4;
-- get the songs id from the albumn
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Black Eyes", 151, "Rock", 5),
("Shallow", 201, "Country, rock, folk rock", 5); -- id albums 5;


INSERT INTO albums (albums_title, date_released, artist_id) VALUES ("Born This Way", "2011=01=01", 4); --6 id album;
-- get the songs id from the albumn
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Born this way", 252, "Electropop", 6);

-- Justine Bieber
INSERT INTO albums (albums_title, date_released, artist_id) VALUES ("Purpose", "2015-01-01", 5); -- album id 7;
-- get the songs id from the albumn
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Sorry", 200, "Dancehall-poptropical housemoombathon", 7);


-- Advance selects
-- Excluding records (!=)
SELECT * FROM songs WHERE id !=1;


-- Greater than
SELECT * FROM songs WHERE id > 1;

-- less than
SELECT * FROM songs WHERE id < 9;

-- equal
SELECT * FROM songs WHERE id = 1;


-- OR
SELECT * FROM songs WHERE id = 1 OR id = 5;

-- IN
SELECT * FROM songs WHERE  id IN (1, 4, 5); --get the specific id from songs
SELECT * FROM songs WHERE genre IN ("K-pop", "Rock"); -- get the specific genre from songs (for duplicate genre)


-- Combining Conditions
SELECT * FROM songs WHERE genre = "Rock" AND length > 150;

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%s"; --Find the song that end in the letter s 
SELECT * FROM songs WHERE song_name LIKE "s%"; --Find the song that start in the letter s

SELECT * FROM songs WHERE song_name LIKE "%s%"; --Find the song name in between in the letter s

SELECT * FROM albums WHERE date_released LIKE "201_-01-01"; --Find the year of released
SELECT * FROM songs WHERE song_name LIKE "b%" AND song_name LIKE "%r%";

-- Finding song_name with second character e
SELECT * FROM songs WHERE song_name LIKE "_e%";


-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC; -- a-z, 1-10
SELECT * FROM songs ORDER BY song_name DESC; --z-a, 10-1


-- Distinct
SELECT DISTINCT genre FROM songs; -- is to find to song name without duplicate

-- Table Joins
SELECT * FROM artist JOIN albums ON artist.id = albums.artist_id;
/* SELECT * FROM ReferenceTable JOIN tableName ON ReferenceTable.PrimaryKey = TableName.foreignKey */

SELECT * FROM albums
    JOIN artist ON albums.artist_id = artist.id;

-- specify selection table only
SELECT albums_title, artist.name FROM albums
    JOIN artist ON albums.artist_id = artist.id;


-- Combine more than two tables
SELECT * FROM artist JOIN albums ON artist.id = albums.artist_id
JOIN songs ON albums.id = songs.albums_id;

/* Select * FROM Table */

-- specify selection table only
SELECT songs.song_name FROM artist JOIN albums ON artist.id = albums.artist_id
JOIN songs ON albums.id = songs.albums_id;

-- Select columns to be included per table
SELECT artist.name, albums.albums_title FROM artist JOIN albums ON artist.id = albums.artist_id;


/*Union */
SELECT artist.name FROM artist UNION
SELECT albums.albums_title FROM albums;


-- inner join mean have reference primary
SELECT * FROM artist JOIN albums ON artist.id = albums.artist_id;

-- Left join
SELECT * FROM artist Left JOIN albums ON artist.id = albums.artist_id;

-- Right join
SELECT * FROM albums
    RIGHT JOIN artist ON albums.artist_id = artist.id;

SHOW DATABASES;

-- Create a database
CREATE DATABASE music_db;


-- delete database
DROP DATABASE music_db;

-- delete or drop table in the database
DROP TABLE users;


-- select a database
USE music_db;
-- CREATE A TABLE
Create Table users_account (
	id INT not null AUTO_INCREMENT,  -- AUTO_INCREMENT CAN CAOUNT DIFFERENTLY
	username VARCHAR(50) not null,
	password VARCHAR(50) not null,
	full_name VARCHAR(50) not null,
	contact_number INT not null,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY(id)

);


CREATE TABLE artist (
	id INT not null AUTO_INCREMENT,
	name VARCHAR(50) not null,
	PRIMARY KEY (id)
);

CREATE TABLE albums (
	id INT not null AUTO_INCREMENT,
	albums_title VARCHAR(50) not null,
	date_released DATE not null,
	artist_id INT not null,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
	FOREIGN KEY (artist_id) REFERENCES artist(id)
		ON UPDATE CASCADE
		ON delete restrict
);


CREATE TABLE songs(
	id INT not null AUTO_INCREMENT,
	song_name VARCHAR(50) not null,
	length Time not null,
	genre VARCHAR(50) not null,
	albums_id INT not null,
	PRIMARY key (id),
	CONSTRAINT fk_songs_albums_id
	FOREIGN KEY(albums_id) REFERENCES albums(id)
	ON UPDATE CASCADE
	ON delete restrict
);

CREATE TABLE playlist(
	id INT not null AUTO_INCREMENT,
	user_id int not null,
	datetime_created DATETIME not null,
	PRIMARY key(id),
	CONSTRAINT fk_playlist_user_id
		FOREIGN key(user_id) REFERENCES user_accounts(id)
		ON UPDATE CASCADE
		ON delete restrict
);

CREATE TABLE playlist_songs(
	id INT not null AUTO_INCREMENT,
	playlist_id INT not null,
	song_id INT not null,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlist_song_playlist_id
		FOREIGN key(playlist_id) REFERENCES playlist(id)
		ON UPDATE CASCADE
		ON delete restrict,
		CONSTRAINT fk_playlist_songs_song_id
		FOREIGN key(song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON delete restrict

);
/*C- create */
-- insertion of name in the table of artist
INSERT INTO artist (name) VALUES ("Rivermaya");
INSERT INTO artist (name) VALUES ("Psy");

-- insert multiple columns
INSERT INTO albums (albums_title, date_released, artist_id) VALUES ("Psy 6", "2021-1-1", 2);
INSERT INTO albums (albums_title, date_released, artist_id) VALUES ("Trip", "1991-1-1", 1);

-- insertion of songs table
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Gangnam style", 253, "K-pop", 1);
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Kisapmata", 230, "OPM", 2);


/* R- retrieve */
/* Retrieve all column values of songs table */ 
SELECT * FROM songs;  -- * means all

/* Display the song_name and genre of all songs */
SELECT song_name, genre FROM songs;

-- Mini Activity
SELECT song_name, albums_id FROM songs;



-- Display the song_name of all the OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";
SELECT song_name, genre FROM songs WHERE genre = "OPM";


/* U - update */
-- updating command
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

-- can use AND or OR for multiple condition
UPDATE songs SET length = 240 WHERE song_name = "Kundiman" AND genre = "K-pop" ;


/*D - Deletion */
DELETE FROM songs WHERE genre = "OPM" AND length > 200;
-- in where class we could add conditions using AND or OR


